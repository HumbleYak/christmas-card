local snowflake_size
local snowflakes = {}
local app_width
local app_height

local num_snowflakes = 186

local num_snowflake1 = 32
local size_max1 = 45
local size_min1 = 18

local num_snowflake2 = 64
local size_max2 = 26
local size_min2 = 6

local timer1 = nil
local timer2 = nil

local layer2_enabled = true
if(layer2_enabled == false) then
  num_snowflake1 = num_snowflakes
end

local function round(num, idp)
    local mult = 10^(idp or 0)
    if num >= 0 then return math.floor(num * mult + 0.5) / mult
    else return math.ceil(num * mult - 0.5) / mult end
end

function Mike_CBInit(mapargs)
  math.randomseed(os.time())
  math.random(1,100)
  
  local env = gre.env({"screen_width", "screen_height"})
  app_width = env["screen_width"]
  app_height = env["screen_height"]
  
  gre.move_control("MikeMainLayer1.snowflake_template1",-app_width,-app_height)
  gre.move_control("MikeMainLayer2.snowflake_template2",-app_width,-app_height)
  
  Mike_preloadSnowflakes()
  for i=1,num_snowflake1 do
  
    local data = {}
    data["x"] = -app_width
    data["y"] = -app_height
    gre.clone_control("snowflake_template1","snowflake"..i,"MikeMainLayer1",data)
    
    gre.set_value("MikeMainLayer1.snowflake"..i..".image", gre.SCRIPT_ROOT .. "/../images/snowflakes/snowflake-"..i..".png")
  end
    
  if(layer2_enabled) then
    for i=1,num_snowflake2 do
      local data = {}
      data["x"] = -app_width
      data["y"] = -app_height
      gre.clone_control("snowflake_template2","snowflake"..i,"MikeMainLayer2",data)
      
      gre.set_value("MikeMainLayer2.snowflake"..i..".image", gre.SCRIPT_ROOT .. "/../images/snowflakes/snowflake-"..i..".png")
    end
  end
end

function Mike_CBStopTimers(mapargs)
  if(timer1) then
    gre.timer_clear_interval(timer1)
    timer1 = nil
  end
  
  if(timer2) then
    gre.timer_clear_interval(timer2)
    timer2 = nil
  end
end

function Mike_CBStartSnowflakes(mapargs)
  local snowflake1_num = 0
  local snowflake2_num = 0
  local snowflake1 = 0
  local snowflake2 = 64
  
  timer1 = gre.timer_set_interval(function ()
                             local basename = "MikeMainLayer1.snowflake" .. snowflake1_num
                             local duration = 2000 + math.random(0, 500)
                             local animation = Mike_createSnowflakeAnimation(basename, duration, snowflake1, size_min1, size_max1)
                             gre.animation_trigger(animation, 
                                                    {context="MikeMainLayer1.snowflake"..snowflake1_num, id="MikeMainLayer1.snowflake"..snowflake1_num})
                             snowflake1_num = snowflake1_num + 1
                             if(snowflake1_num >= num_snowflake1) then
                               snowflake1_num = 0
                             end
                             
                             snowflake1 = snowflake1 + 1
                             if(snowflake1 >= num_snowflakes) then
                               snowflake1 = 0
                             end  
                           end, 300)
                           
  timer2 = gre.timer_set_interval(function ()          
                             if(layer2_enabled) then        
                               local basename = "MikeMainLayer2.snowflake" .. snowflake2_num
                               local duration = 3000 + math.random(0, 500) 
                               local animation = Mike_createSnowflakeAnimation(basename, duration, snowflake2, size_min2, size_max2)
                               gre.animation_trigger(animation, 
                                                      {context="MikeMainLayer2.snowflake"..snowflake2_num, id="MikeMainLayer2.snowflake"..snowflake2_num})
                             end
                             snowflake2_num = snowflake2_num + 1
                             if(snowflake2_num >= num_snowflake2) then
                               snowflake2_num = 0
                             end 
                             
                             snowflake2 = snowflake2 + 1
                             if(snowflake2 >= num_snowflakes) then
                               snowflake2 = 0
                             end  
                          end,80)
end

function Mike_createSnowflakeAnimation(basename, duration, i, size_min, size_max, cb)
  local data = {}
  local name = gre.animation_create(60,1,cb)
  
  local size = round(math.random(size_min, size_max))
  duration = duration + (1500-(1500 * size / size_max1))
  local x = math.random(-size,app_width)

  local num = math.random(0,55)
  if(num >= 50 ) then
    i = 186
  end
  
  data = {}
  data["key"] = basename .. ".grd_hidden" 
  data["rate"] = "linear"
  data["duration"] = 0
  data["offset"] = 0
  data["to"] = 0
  gre.animation_add_step(name,data)
  
  data = {}
  data["key"] = basename .. ".grd_hidden" 
  data["rate"] = "linear"
  data["duration"] = 0
  data["offset"] = duration
  data["to"] = 1
  gre.animation_add_step(name,data)
  
  data = {}
  data["key"] = basename .. ".image" 
  data["rate"] = "linear"
  data["duration"] = 0
  data["offset"] = 0
  data["to"] = gre.SCRIPT_ROOT .. "/../images/snowflakes/snowflake-"..i..".png"
  gre.animation_add_step(name,data)

  data = {}
  data["key"] = basename .. ".grd_width"
  data["rate"] = "linear"
  data["duration"] = 0
  data["offset"] = 0
  data["to"] = size
  gre.animation_add_step(name,data)
  
  data = {}
  data["key"] = basename .. ".grd_height"
  data["rate"] = "linear"
  data["duration"] = 0
  data["offset"] = 0
  data["to"] = size
  gre.animation_add_step(name,data)
  
  data = {}
  data["key"] = basename .. ".grd_x"
  data["rate"] = "snowx"
  data["duration"] = duration
  data["offset"] = 0
  data["from"] = x 
  data["to"] = x + math.random(-50,50)
  gre.animation_add_step(name,data)
  
  data = {}
  data["key"] = basename .. ".grd_y"
  data["rate"] = "snow"
  data["duration"] = duration
  data["offset"] = 0
  data["from"] = -size 
  data["to"] = app_height
  gre.animation_add_step(name,data)
  
  data = {}
  data["key"] = basename .. ".angle_y"
  data["rate"] = "linear"
  data["duration"] = duration
  data["offset"] = 0
  data["from"] = math.random(-180, 180)
  data["to"] = math.random(-360,360)
  gre.animation_add_step(name,data)
  
  data = {}
  data["key"] = basename .. ".angle_x"
  data["rate"] = "linear"
  data["duration"] = duration
  data["offset"] = 0
  data["from"] = math.random(-180, 180)
  data["to"] = math.random(-360,360)
  gre.animation_add_step(name,data)
  
  data = {}
  data["key"] = basename .. ".angle_z"
  data["rate"] = "linear"
  data["duration"] = duration
  data["offset"] = 0
  data["from"] = math.random(-180, 180)
  data["to"] = math.random(-360,360)
  gre.animation_add_step(name,data)
  
  return name
end

function Mike_preloadSnowflakes()
  for i=0,num_snowflakes do
    gre.load_resource("image", gre.SCRIPT_ROOT .. "/../images/snowflakes/snowflake-"..i..".png")
  end
end 

----------------------------------------------------------
--MikeSecondaryScreen
local colors =  {
                  0x248924,
                  0xAC1A00, 
                  0xFCFC00 
                }
local activeColors = {
                        color1=1,
                        color2=2,
                        color3=3                        
                      }
                      
local offset = 250
local duration = 750

local function linearTween(time, start_value, end_value, duration)
  return start_value + (end_value - start_value) * time/duration
end

local function easeinTween(time, start_value, end_value, duration)
  local t = time/duration
  t = t*t*t*t*t
  local c = end_value - start_value
  return start_value + (c*t)
end

local function easeoutTween(time, start_value, end_value, duration)
  local t = time/duration - 1
  t = t*t*t*t*t + 1
  local c = end_value - start_value
  return start_value + (c*t)
end

local function easeinoutTween(time, start_value, end_value, duration)
  local t = time
  local c = end_value - start_value

  t = t / (duration/2)
  if(t < 1) then
    t = t*t*t*t*t;
    return start_value + (c/2*t)
  end
  
  t = t - 2
  t = t*t*t*t*t + 2;
  
  return start_value + (c/2*t)
end

local function bounceTween(time, start_value, end_value, duration)
  local t = time/duration
  local c = end_value - start_value

  if (t < 1/2.75) then
    return c*(7.5625*t*t) + start_value
  elseif (t < 2/2.75) then
    t = t - 1.5/2.75
    local postFix = t
    return c*(7.5625*t*t + 0.75) + start_value
  elseif (t < 2.5/2.75) then
    t = t - 2.25/2.75
    return c*(7.5625*t*t + 0.9375) + start_value
  else
    t = t - 2.625/2.75
    local postFix = t
    return c*(7.5625*t*t + 0.984375) + start_value
  end
end

local availableColorTweens = {
                               linear=linearTween,
                               easein=easeinTween,
                               easeout=easeoutTween,
                               easeinout=easeinoutTween,
                               bounce=bounceTween
                             }
local activeTween = availableColorTweens.easeout

local lightbulb_animation

local function nextColor(color)
  local i = activeColors[color]
  if(i == 3) then
    i = 1
  else
    i = i + 1
  end
  activeColors[color] = i
end

function Mike_CBScreen2ShowPre(mapargs)
  gre.animation_create_tween("variabletween_color", Mike_CBColorTween)
  
  lightbulb_animation = Mike_CreateGenericLightBulbAnimation()

  gre.set_value("MikeSecondaryLayer1.color1", colors[activeColors.color1])
  nextColor("color1")
  gre.set_value("MikeSecondaryLayer1.dest_color1", colors[activeColors.color1])
  
  gre.set_value("MikeSecondaryLayer1.color2", colors[activeColors.color2])
  nextColor("color2")
  gre.set_value("MikeSecondaryLayer1.dest_color2", colors[activeColors.color2])
  
  gre.set_value("MikeSecondaryLayer1.color3", colors[activeColors.color3])
  nextColor("color3")
  gre.set_value("MikeSecondaryLayer1.dest_color3", colors[activeColors.color3])
end

function Mike_CBScreen2ShowPost(mapargs)
  gre.animation_trigger(lightbulb_animation)
end

function Mike_CBBulbAnimationComplete()
  nextColor("color1")
  gre.set_value("MikeSecondaryLayer1.dest_color1", colors[activeColors.color1])
  
  nextColor("color2")
  gre.set_value("MikeSecondaryLayer1.dest_color2", colors[activeColors.color2])
  
  nextColor("color3")
  gre.set_value("MikeSecondaryLayer1.dest_color3", colors[activeColors.color3])
  
  gre.animation_trigger(lightbulb_animation)
end

function Mike_CreateGenericLightBulbAnimation()
  local name = gre.animation_create(60,0, Mike_CBBulbAnimationComplete)
  local data = {}
  
  data["key"] = "${app:MikeSecondaryLayer1.color1}"
  data["rate"] = "variabletween_color"
  data["duration"] = duration
  data["offset"] = offset
  data["to"] = "${app:MikeSecondaryLayer1.dest_color1}"
  gre.animation_add_step(name,data)
  
  data["key"] = "${app:MikeSecondaryLayer1.color2}"
  data["rate"] = "variabletween_color"
  data["duration"] = duration
  data["offset"] = offset
  data["to"] = "${app:MikeSecondaryLayer1.dest_color2}"
  gre.animation_add_step(name,data)
  
  data["key"] = "${app:MikeSecondaryLayer1.color3}"
  data["rate"] = "variabletween_color"
  data["duration"] = duration
  data["offset"] = offset
  data["to"] = "${app:MikeSecondaryLayer1.dest_color3}"
  gre.animation_add_step(name,data)
  
  return name
end


function Mike_CBColorTween(time, start_value, diff, duration)
  local end_value = start_value + diff

  local rStart = bit32.arshift(bit32.band(start_value, 0xff0000), 16)
  local rEnd = bit32.arshift(bit32.band(end_value, 0xff0000), 16)
  local r = round(activeTween(time, rStart, rEnd, duration))
  
  local gStart = bit32.arshift(bit32.band(start_value, 0x00ff00), 8)
  local gEnd = bit32.arshift(bit32.band(end_value, 0x00ff00), 8)
  local g = round(activeTween(time, gStart, gEnd, duration))
  
  local bStart = bit32.band(start_value, 0x0000ff)
  local bEnd = bit32.band(end_value, 0x0000ff)
  local b = round(activeTween(time, bStart, bEnd, duration))
  
  r = bit32.arshift(r, -16)
  g = bit32.arshift(g, -8) 

  return bit32.bor(r, g, b)
end

function Mike_CBSelectRate(mapargs) 
  local rate = mapargs.rate
  
  if(activeTween == availableColorTweens[rate]) then
    return
  end
  
  activeTween = availableColorTweens[rate]
  
  local data = {}
  for i=1,5 do
    data["MikeSecondaryLayer1.rate"..i..".color"] = 0xffffff
  end
  data[mapargs.context_control..".color"] = 0xfe0f0f 
  gre.set_data(data)
end
